# Python Flask API with Elasticsearch

An example of REST API with Flask and Elasticsearch as backend.

Install docker, docker-compose

```bash
docker-compose up -d
```

Then go to `http://localhost:5100/info` to access elasticsearch info page
